package com.example.oleg.imageviewer.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.dao.ImageDao;
import com.example.oleg.imageviewer.models.Photo;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SplashScreenActivity extends AppCompatActivity {

    SimpleDateFormat dateParser = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss", Locale.US);

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (Build.VERSION.SDK_INT < 23 || checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED) {
            ImageLoaderAsyncTask imageLoaderAsyncTask = new ImageLoaderAsyncTask();
            imageLoaderAsyncTask.execute();
        } else {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImageLoaderAsyncTask imageLoaderAsyncTask = new ImageLoaderAsyncTask();
                    imageLoaderAsyncTask.execute();
                } else {
                    Toast.makeText(SplashScreenActivity.this, R.string.errorNoAccesss, Toast.LENGTH_SHORT)
                            .show();
                    goToMainActivity();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    private List<Photo> getDeviceImages() {
        final String[] projection = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.MIME_TYPE};
        final String selection = MediaStore.Images.Media.MIME_TYPE + " = ? ";
        final String[] args = {"image/jpeg"};
        final Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                args,
                MediaStore.Images.Media.DATE_TAKEN + " DESC");
        List<Photo> images = new ArrayList<>(cursor.getCount());

        Date d;

        while (cursor.moveToNext()) {
            Photo image = new Photo();
            image.setBasename(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
            image.setTitle(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME)));
            if (image.getBasename() == null) {
                continue;
            }
            String[] temp = image.getBasename().split("\\.");
            image.setExtension(temp[temp.length - 1]);
            try {
                ExifInterface exifInterface = new ExifInterface(image.getBasename());
                String takenAt = exifInterface.getAttribute(ExifInterface.TAG_DATETIME);
                if (takenAt != null) {
                    d = dateParser.parse(takenAt);
                    image.setTakenat(d);
                } else {
                    image.setTakenat(new Date(new File(image.getBasename()).lastModified()));
                }
            } catch (Exception e) {
                image.setTakenat(new Date(new File(image.getBasename()).lastModified()));
            }

            image.setChecksum(getSHAFromFile(image.getBasename()));
            images.add(image);
        }
        return images;
    }

    class ImageLoaderAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            List<Photo> deviceImages = getDeviceImages();
            ImageDao imageDao = new ImageDao(SplashScreenActivity.this);
            for (Photo image : deviceImages) {
                if (imageDao.checkIfExist(image.getChecksum(),image.getBasename())) {
                    continue;
                } else {
                    imageDao.saveImage(image);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            goToMainActivity();
        }


    }

    private void goToMainActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashScreenActivity.this.finish();
    }

    public static String getSHAFromFile(String fileName) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            InputStream fis = new FileInputStream(fileName);
            int n = 0;
            byte[] buffer = new byte[8192];
            while (n != -1) {
                n = fis.read(buffer);
                if (n > 0) {
                    digest.update(buffer, 0, n);
                }
            }
            byte[] digestResult = digest.digest();
            return String.format("%0" + (digestResult.length * 2) + "X", new BigInteger(1, digestResult)).toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();


            return null;
        }
    }

}