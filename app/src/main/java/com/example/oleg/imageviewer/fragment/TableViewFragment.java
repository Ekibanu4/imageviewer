package com.example.oleg.imageviewer.fragment;

import android.content.Intent;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.activity.SlideShowActivity;
import com.example.oleg.imageviewer.adapter.GridAdapter;

import com.example.oleg.imageviewer.dao.ImageDao;
import com.example.oleg.imageviewer.models.Photo;

import java.util.List;

public class TableViewFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tableview, null);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.imageRecycleView);


        ImageDao imageDao = new ImageDao(getActivity());
        final List<Photo> images = imageDao.getImages(false);

        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = (int) (size.x / 3);


        GridAdapter gridAdapter = new GridAdapter(getActivity(), R.layout.grid_item, images, width, new GridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Photo item) {
                Intent intent = new Intent(getActivity(), SlideShowActivity.class);
                intent.putExtra("currentPosition", images.indexOf(item));
                intent.putExtra("hidden", "0");
                startActivityForResult(intent, 1);
            }
        });
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setAdapter(gridAdapter);
        recyclerView.setLayoutManager(layoutManager);

        return view;
    }
}
