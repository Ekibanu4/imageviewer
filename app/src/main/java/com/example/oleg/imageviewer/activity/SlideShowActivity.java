package com.example.oleg.imageviewer.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;


import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.dao.ImageDao;
import com.example.oleg.imageviewer.fragment.SlideShowFragment;
import com.example.oleg.imageviewer.models.Photo;

import java.util.List;

/**
 * Created by OLEG on 16.07.2016.
 */
public class SlideShowActivity extends AppCompatActivity {
    boolean makeChanges = false;
    List<Photo> images = null;
    ViewPager pager;
    MyFragmentPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slideshow);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        String hidden = intent.getStringExtra("hidden");

        boolean h = false;
        if (hidden.equals("1")) {
            h = true;
        }

        int currentPosition = getIntent().getIntExtra("currentPosition", 0);

        final TextView textViewNothingToShow = (TextView) findViewById(R.id.textViewNothingToShow);

        final Button hideButton = (Button) findViewById(R.id.hidebuttontooldar);
        final Button shareButton = (Button) findViewById(R.id.shareButton);
        if (h) {
            hideButton.setText(R.string.toolbarUnhideButton);

        } else {
            hideButton.setText(R.string.tollbarHideButton);

        }

        final boolean finalH = h;
        hideButton.setOnClickListener(new View.OnClickListener() {

                                          @Override
                                          public void onClick(View view) {
                                              makeChanges = true;
                                              int i = pager.getCurrentItem();
                                              int ID = images.get(pager.getCurrentItem()).getID();
                                              boolean hide = !images.get(pager.getCurrentItem()).isHidden();
                                              ImageDao imageDao = new ImageDao(getApplicationContext());
                                              imageDao.updateImageHideState(ID, hide);
                                              images.remove(i);
                                              if (images.isEmpty()) {
                                                  hideButton.setVisibility(View.GONE);
                                                  shareButton.setVisibility(View.GONE);
                                                  textViewNothingToShow.setVisibility(View.VISIBLE);
                                                  if (finalH) {
                                                      textViewNothingToShow.setText(R.string.nohiddenImagesToast);
                                                  } else {
                                                      textViewNothingToShow.setText(R.string.noImagesToast);
                                                  }
                                              }
                                              pagerAdapter.notifyDataSetChanged();
                                          }
                                      }

        );


        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                int i = pager.getCurrentItem();
                String path = "file://" + images.get(pager.getCurrentItem()).getBasename();
                sharingIntent.setType("image/jpeg");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.imagesendtitle));
                sharingIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.lookatthis));
                sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.shareimagechoser)));
            }
        });


        ImageDao imageDao = new ImageDao(this);
        images = imageDao.getImages(h);
        if (images.isEmpty())

        {
            if (h) {
                hideButton.setVisibility(View.GONE);
                textViewNothingToShow.setText(R.string.nohiddenImagesToast);
            } else {
                hideButton.setVisibility(View.GONE);
                textViewNothingToShow.setText(R.string.noImagesToast);
            }

        } else

        {
            pager = (ViewPager) findViewById(R.id.slideshowViewPager);
            pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
            pager.setAdapter(pagerAdapter);
            pager.setCurrentItem(currentPosition);
        }
    }

    private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return SlideShowFragment.newInstance(position, images);
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("changes", makeChanges);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }


}
