package com.example.oleg.imageviewer.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.helper.ImgLoader;
import com.example.oleg.imageviewer.models.Photo;
import java.util.List;

/**
 * Created by OLEG on 16.07.2016.
 */
public class SlideShowFragment extends Fragment {
    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    static List<Photo> images;

    int pageNumber;

    public static SlideShowFragment newInstance(int page, List<Photo> _images) {
        images = _images;
        SlideShowFragment pageFragment = new SlideShowFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slideshow, null);
        final ImageView imageView = (ImageView) view.findViewById(R.id.photoImageView);
        ImgLoader imgLoader = new ImgLoader();
        imgLoader.loadimage(images.get(pageNumber).getBasename(),imageView,getActivity());
        return view;
    }


}



