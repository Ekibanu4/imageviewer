package com.example.oleg.imageviewer.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;


import com.example.oleg.imageviewer.models.Photo;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by OLEG on 16.07.2016.
 */
public class ImageDao {


    DbHelper dbHelper;
    Context context;
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    SQLiteDatabase db;

    public ImageDao(Context context) {
        this.context = context;
        this.dbHelper = new DbHelper(context);
    }

    public void saveImage(Photo photo) {
        ContentValues cv = new ContentValues();
        db = dbHelper.getWritableDatabase();
        cv.put("basename", photo.getBasename());
        cv.put("title", photo.getTitle());
        cv.put("checksum", photo.getChecksum());
        cv.put("extension", photo.getExtension());
        cv.put("hidden", false);
        cv.put("takenat", df.format(photo.getTakenat()));
        db.insert("images", null, cv);
        db.close();
    }

    public List<Photo> getImages(boolean hidden) {
        int hide = 0;
        if (hidden) {
            hide = 1;
        }

        db = dbHelper.getWritableDatabase();
        final String[] projection = {"ID", "basename", "title", "checksum", "extension", "takenat"};
        final String selection = "hidden =?";
        String[] selectionArgs = new String[]{String.valueOf(hide)};
        Cursor c = db.query("images", projection, selection, selectionArgs, null, null, "takenat desc");

        List<Photo> images = new ArrayList<>();

        while (c.moveToNext()) {
            File file = new File(c.getString(c.getColumnIndex("basename")));
            if(!file.exists()) {
                int ID = c.getInt(c.getColumnIndex("ID"));
                db.delete("images", "id = " + ID, null);
            } else {
                Photo photo = new Photo();
                photo.setID(c.getInt(c.getColumnIndex("ID")));
                photo.setTitle(c.getString(c.getColumnIndex("title")));
                photo.setBasename(c.getString(c.getColumnIndex("basename")));
                photo.setChecksum(c.getString(c.getColumnIndex("checksum")));
                photo.setExtension(c.getString(c.getColumnIndex("extension")));
                try {
                    photo.setTakenat(df.parse(c.getString(c.getColumnIndex("takenat"))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                photo.setHidden(hidden);
                images.add(photo);
            }
        }
        db.close();
        return images;

    }

    public boolean checkIfExist(String checksum,String basename) {
        db = dbHelper.getWritableDatabase();
        final String[] projection = {"ID"};
        final String selection = "checksum=? and basename=?";
        String[] args = new String[]{checksum, basename};
        int size = 0;
        Cursor c = db.query("images", projection, selection, args, null, null, null);
        if (c.moveToNext()) {
            size = c.getCount();
        }
        db.close();
        return size > 0;
    }

    public void updateImageHideState(int id, boolean hide) {
        db = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        if (hide) {
            cv.put("hidden", 1);
            db.update("images", cv, "ID = " + id, null);
        } else {
            cv.put("hidden", 0);
            db.update("images", cv, "ID = " + id, null);
        }
        db.close();
    }

}

