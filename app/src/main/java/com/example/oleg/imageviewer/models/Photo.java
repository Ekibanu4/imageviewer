package com.example.oleg.imageviewer.models;

import java.util.Date;

/**
 * Created by OLEG on 16.07.2016.
 */
public class Photo {

    private int ID;
    private String basename;
    private String title;
    private String checksum;
    private String extension;
    private boolean hidden;
    private Date takenat;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getBasename() {
        return basename;
    }

    public void setBasename(String basename) {
        this.basename = basename;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Date getTakenat() {
        return takenat;
    }

    public void setTakenat(Date takenat) {
        this.takenat = takenat;
    }
}
