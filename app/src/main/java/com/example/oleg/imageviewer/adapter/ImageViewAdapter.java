package com.example.oleg.imageviewer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.helper.ImgLoader;
import com.example.oleg.imageviewer.models.Photo;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by OLEG on 16.07.2016.
 */
public class ImageViewAdapter extends ArrayAdapter<Photo> {

    SimpleDateFormat dateParser = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
    Context context;
    int resource;
    List<Photo> images;
    int width, height;

    public ImageViewAdapter(Context context, int resource, List<Photo> images, int width, int height) {
        super(context, resource, images);

        this.context = context;
        this.resource = resource;
        this.images = images;
        this.width = width;
        this.height = height;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PhotoHolder photoHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resource, parent, false);

            photoHolder = new PhotoHolder();
            photoHolder.photoImageView = (ImageView) convertView.findViewById(R.id.photoImageView);
            photoHolder.takenAtTextView = (TextView) convertView.findViewById(R.id.takenAtTextVeiw);
            photoHolder.titleTextView = (TextView) convertView.findViewById(R.id.titleTextView);
            convertView.setTag(photoHolder);

        } else {
            photoHolder = (PhotoHolder) convertView.getTag();
        }

        final Photo photo = images.get(position);
        if (photo != null) {

            photoHolder.photoImageView.setImageResource(R.drawable.ic_menu_gallery);
            ImgLoader imgLoader = new ImgLoader();
            imgLoader.loadimage(images.get(position).getBasename(),photoHolder.photoImageView,context);
            photoHolder.titleTextView.setText(photo.getTitle());
            photoHolder.takenAtTextView.setText(dateParser.format(photo.getTakenat()));

        }
        return convertView;

    }

    static class PhotoHolder {
        ImageView photoImageView;
        TextView titleTextView;
        TextView takenAtTextView;
    }

}
