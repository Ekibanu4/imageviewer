package com.example.oleg.imageviewer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.helper.ImgLoader;
import com.example.oleg.imageviewer.models.Photo;

import java.util.List;


/**
 * Created by OLEG on 16.07.2016.
 */
public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Photo item);
    }

    Context context;
    int resource;
    List<Photo> images;
    int width, height;

    private final OnItemClickListener listener;

    public GridAdapter(Context context, int resource, List<Photo> images, int width, OnItemClickListener listener) {
        this.context = context;
        this.resource = resource;
        this.images = images;
        this.height = width;
        this.width = width;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(resource, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bind(images.get(position), listener);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, width);
        holder.photoImageView.setLayoutParams(params);
        final Photo photo = images.get(position);
        if (photo != null) {
            holder.photoImageView.setImageResource(R.drawable.ic_menu_gallery);

            ImgLoader imgLoader = new ImgLoader();
            imgLoader.loadimage(images.get(position).getBasename(), holder.photoImageView, context);
        }
    }


    @Override
    public int getItemCount() {
        return images.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView photoImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            photoImageView = (ImageView) itemView.findViewById(R.id.photoImageView);
        }

        public void bind(final Photo item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}