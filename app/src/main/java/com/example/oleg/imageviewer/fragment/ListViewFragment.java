package com.example.oleg.imageviewer.fragment;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.activity.SlideShowActivity;
import com.example.oleg.imageviewer.adapter.ImageViewAdapter;
import com.example.oleg.imageviewer.dao.ImageDao;
import com.example.oleg.imageviewer.models.Photo;

import java.util.List;

public class ListViewFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cardview, null);

        ListView imagesListView = (ListView) view.findViewById(R.id.imagesListView);


        ImageDao imageDao = new ImageDao(getActivity());
        final List<Photo> images = imageDao.getImages(false);

        imagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), SlideShowActivity.class);
                intent.putExtra("currentPosition",i);
                intent.putExtra("hidden","0");
                startActivityForResult(intent,1);
            }
        });

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        final int width = (int) ((size.x)/8);


        ImageViewAdapter imageViewAdapter = new ImageViewAdapter(getActivity(),R.layout.list_item,images, width, width);
        imagesListView.setAdapter(imageViewAdapter);

        return view;
    }
}


