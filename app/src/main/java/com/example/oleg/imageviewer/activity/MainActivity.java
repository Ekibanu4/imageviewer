package com.example.oleg.imageviewer.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.oleg.imageviewer.dao.ImageDao;
import com.example.oleg.imageviewer.fragment.CardViewFragment;
import com.example.oleg.imageviewer.fragment.ListViewFragment;
import com.example.oleg.imageviewer.R;
import com.example.oleg.imageviewer.fragment.TableViewFragment;
import com.example.oleg.imageviewer.models.Photo;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager fragmentManager;
    CardViewFragment cardViewFragment;
    TableViewFragment tableViewFragment;
    ListViewFragment listViewFragment;
    FrameLayout container;
    Intent intent;
    final static String TAG_CARD = "CARD";
    final static String TAG_LIST = "LIST";
    final static String TAG_TABLE = "TABLE";
    final String LAST_VIEW = "LAST_VIEW";
    SharedPreferences sPref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardViewFragment = new CardViewFragment();
        tableViewFragment = new TableViewFragment();
        listViewFragment = new ListViewFragment();

        TextView nothingToShowMain = (TextView) findViewById(R.id.nothingToShowMain);

        ImageDao imageDao = new ImageDao(this);
        List<Photo> images = imageDao.getImages(false);
        if (images.isEmpty()) {
            nothingToShowMain.setVisibility(View.VISIBLE);
            nothingToShowMain.setText(R.string.noImagesToast);
        }
        images = null;


        fragmentManager = getSupportFragmentManager();
        container = (FrameLayout) findViewById(R.id.container);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (getLastState()) {
            case TAG_CARD:
                fragmentTransaction.add(R.id.container, cardViewFragment, TAG_CARD);
                break;
            case TAG_LIST:
                fragmentTransaction.add(R.id.container, listViewFragment, TAG_LIST);
                break;
            case TAG_TABLE:
                fragmentTransaction.add(R.id.container, tableViewFragment, TAG_TABLE);
                break;
            default:
                fragmentTransaction.add(R.id.container, tableViewFragment, TAG_TABLE);
                break;
        }

        fragmentTransaction.commit();

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menuExit:
                System.exit(0);
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.ndCardView:
                fragment = (CardViewFragment) fragmentManager.findFragmentByTag(TAG_CARD);
                if (fragment == null) {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, cardViewFragment, TAG_CARD);
                    fragmentTransaction.commit();
                    saveLastState(TAG_CARD);
                }
                break;
            case R.id.ndListView:

                fragment = (ListViewFragment) fragmentManager.findFragmentByTag(TAG_LIST);
                if (fragment == null) {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, listViewFragment, TAG_LIST);
                    fragmentTransaction.commit();
                    saveLastState(TAG_LIST);
                }
                break;
            case R.id.ndTableView:
                fragment = (TableViewFragment) fragmentManager.findFragmentByTag(TAG_TABLE);
                if (fragment == null) {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container, tableViewFragment, TAG_TABLE);
                    fragmentTransaction.commit();
                    saveLastState(TAG_TABLE);
                }
                break;
            case R.id.ndSlideShow:
                intent = new Intent(MainActivity.this, SlideShowActivity.class);
                intent.putExtra("hidden", "0");
                startActivityForResult(intent, 1);
                break;

            case R.id.ndShowHidden:
                intent = new Intent(MainActivity.this, SlideShowActivity.class);
                intent.putExtra("hidden", "1");
                startActivityForResult(intent, 1);
                break;
            case R.id.exit:
                System.exit(0);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        boolean changes = data.getBooleanExtra("changes", false);
        if (changes) {
            finish();
            startActivity(getIntent());
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    private String getLastState() {
        sPref = getPreferences(MODE_PRIVATE);
        String lastView = sPref.getString(LAST_VIEW, "");
        return lastView;
    }

    void saveLastState(String state) {
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(LAST_VIEW, state);
        ed.apply();
    }

}

